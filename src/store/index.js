import Vue from 'vue';
import Vuex from 'vuex';
import * as product from './modules/productModule';
import * as post from './modules/postModule';
import * as cart from './modules/cartModule';

Vue.use(Vuex);

export default new Vuex.Store({
  state: {},
  mutations: {},
  actions: {},
  modules: { product, cart, post }
});
