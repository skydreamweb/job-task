import apiService from '../../services/apiService';

// NOTE: CART STATE
export const state = {
  cartItems: []
};

// NOTE: CART MUTATIONS
export const mutations = {
  SET_CART_ITEMS: (state, payload) => {
    state.cartItems = payload;
  },
  ADD_ITEM: (state, payload) => {
    state.cartItems.push(payload);
  },
  EDIT_ITEM: (state, payload) => {
    state.cartItems = state.cartItems.filter(item => item.id !== payload.id);
    state.cartItems.push(payload);
  },
  REMOVE_ITEM: (state, payload) => {
    const id = payload;
    state.cartItems = state.cartItems.filter(item => item.id !== id);
  }
};

// NOTE: CART ACTIONS
export const actions = {
  // fetch all cart items from db.json http://localhost:3000/cart' using axios
  fetchCartItems: ({ commit }) => {
    return apiService
      .getCartItems()
      .then(res => {
        commit('SET_CART_ITEMS', res.data);
      })
      .catch(err => console.log(err));
  },

  // add item to cart or increase quantity in db.json POST: http://localhost:3000/cart' using axios
  addToCart: (context, payload) => {
    let cartItems = context.getters['cartItems'];

    // LOCAL: cartItems getter
    if (cartItems.length > 0) {
      const item = cartItems.find(item => {
        return (
          item.productId === payload.productId && item.size === payload.size
        );
      });
      // OPTION 1: if there is no item with same product id add and size -> add to cart
      if (!item) {
        return apiService
          .createCartItem(payload)
          .then(res => {
            context.commit('ADD_ITEM', res.data);
          })
          .catch(err => console.log(err));
      } else {
        // OPTION 2: else -> increase quantity
        return apiService
          .editCartItem(item.id, {
            quantity: item.quantity + payload.quantity
          })
          .then(res => {
            context.commit('EDIT_ITEM', res.data);
          })
          .catch(err => console.log(err));
      }
    } else {
      // FROM DB (if getter cartItems is empty) -> fetch from db.json first (for example: on refresh page)
      context.dispatch('fetchCartItems').then(() => {
        cartItems = context.getters['cartItems'];
        const item = cartItems.find(item => {
          return (
            item.productId === payload.productId && item.size === payload.size
          );
        });
        // OPTION 3: if there is no item with same product id add and size -> add to cart
        if (!item) {
          return apiService
            .createCartItem(payload)
            .then(res => {
              context.commit('ADD_ITEM', res.data);
            })
            .catch(err => console.log(err));
        } else {
          // OPTION 4: else -> increase quantity
          return apiService
            .editCartItem(item.id, {
              quantity: item.quantity + payload.quantity
            })
            .then(res => {
              context.commit('EDIT_ITEM', res.data);
            })
            .catch(err => console.log(err));
        }
      });
    }
  },
  // REMOVE ITEM FROM CART in db.json DELETE: http://localhost:3000/cart/:id using axios
  removeFromCart: (context, id) => {
    apiService
      .deleteCartItem(id)
      .then(() => {
        context.commit('REMOVE_ITEM', id);
      })
      .catch(err => console.log(err));
  }
};

// NOTE: CART GETTERS
export const getters = {
  // get all cart items
  cartItems: state => {
    return state.cartItems;
  },
  // calculate total item price
  totalSumByItem: () => item => {
    return item.quantity * item.price;
  },
  // calculate total price of all items in cart
  totalSum: (state, getters) => {
    let total = 0;
    state.cartItems.forEach(item => {
      total += getters.totalSumByItem(item);
    });
    return total;
  },
  // get sum of item quantities
  qty: state => {
    let qty = 0;
    state.cartItems.forEach(item => (qty += item.quantity));
    return qty;
  }
};
