import apiService from '@/services/apiService';

// NOTE: PRODUCT STATE
export const state = { products: [], product: null };

// NOTE: PRODUCTS MUTATIONS
export const mutations = {
  SET_PRODUCTS: (state, products) => {
    state.products = products;
  },
  SET_PRODUCT: (state, product) => {
    state.product = product;
  }
};

// NOTE: PRODUCTS ACTIONS
export const actions = {
  // fetch all products from db.json http://localhost:3000/products' using axios
  fetchProducts: ({ commit }) => {
    return apiService
      .getProducts()
      .then(res => {
        commit('SET_PRODUCTS', res.data);
      })
      .catch(err => console.log(err));
  },
  fetchProduct: (context, id) => {
    const product = context.getters.product(id);
    // if product is already in product getter
    if (product) {
      context.commit('SET_PRODUCT', product);
      return product;
    } else {
      // if product getter is empty fetch product by id from db.json http://localhost:3000/products/:id' using axios
      return apiService
        .getProduct(id)
        .then(res => {
          context.commit('SET_PRODUCT', res.data);
          return res.data;
        })
        .catch(err => console.log(err));
    }
  }
};

// NOTE: PRODUCTS GETTERS
export const getters = {
  // get all products
  products: state => {
    return state.products;
  },
  // HACK: get product by id -> we can write it also like this:
  // product(state) {
  //   return function(id){
  //     return state.products.find(product => Number(product.id) === Number(id));
  //   }
  // }
  product: state => id => {
    return state.products.find(product => Number(product.id) === Number(id));
  },
  categories: state => {
    const products = state.products;
    // get all different categories of product from DB
    const categories = [];
    // loop trougth the array of product objects ***
    products.forEach(product => {
      if (!categories.includes(product.category)) {
        categories.push(product.category);
      }
    });
    return categories;
  },
  // get all different sizes of product from DB
  sizes: state => {
    const products = state.products;
    const sizes = [];
    products.forEach(product => {
      product.sizes.forEach(size => {
        if (!sizes.includes(size)) {
          sizes.push(size);
        }
      });
    });
    return sizes;
  },
  // get min price of products from DB
  minPrice: state => {
    const products = state.products;
    // Price checking - if there are products in products
    let minPrice = products[0];
    products.forEach(product => {
      if (product.price < minPrice) minPrice = product.price;
    });
    return minPrice;
  },
  // get max price of products from DB
  maxPrice: state => {
    const products = state.products;
    let maxPrice = 0;
    products.forEach(product => {
      if (product.price > maxPrice) maxPrice = product.price;
    });
    return maxPrice;
  },
  // get all brands of products from DB
  brands: state => {
    const products = state.products;
    const brands = [];
    products.forEach(product => {
      if (!brands.includes(product.brand)) {
        brands.push(product.brand);
      }
    });
    return brands;
  }
};
