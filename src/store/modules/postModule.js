import apiService from '@/services/apiService.js';

// NOTE: POST STATE
export const state = {
  posts: [],
  post: {}
};

// NOTE: POST MUTATIONS
export const mutations = {
  SET_POSTS(state, posts) {
    state.posts = posts;
  },
  SET_POST(state, post) {
    state.post = post;
  }
};

// NOTE: POST ACTIONS
export const actions = {
  // fetch all posts from db.json http://localhost:3000/posts' using axios
  fetchPosts(context) {
    return apiService
      .getPosts()
      .then(response => {
        context.commit('SET_POSTS', response.data);
      })
      .catch(err => console.log(err));
  },
  fetchPost(context, id) {
    const post = context.getters.post(id);
    // if post is already in post getter
    if (post.length > 0) {
      context.commit('SET_POST', post[0]);
      return post[0];
    } else {
      // if post getter is empty fetch post by id from db.json http://localhost:3000/posts/:id' using axios
      return apiService
        .getPost(id)
        .then(response => {
          context.commit('SET_POST', response.data);
          return response.data;
        })
        .catch(err => console.log(err));
    }
  },
  // sending comments on post on db.json PATCH: http://localhost:3000/posts/:id' using axios
  editPost(context, payload) {
    return apiService
      .editPost(payload)
      .then(response => {
        context.commit('SET_POST', response.data);
      })
      .catch(err => console.log(err));
  }
};

// NOTE: POST GETTERS
export const getters = {
  // get all posts
  posts: state => {
    return state.posts;
  },
  // HACK: get post by id -> we can write it also like this:
  // post(state) {
  //   return function(id){
  //     return state.posts.find(post => post.id === id);
  //   }
  // }
  post: state => id => {
    const post = state.posts.filter(post => post.id === id);
    return post;
  }
};
