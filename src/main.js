import Vue from 'vue';
import App from './App.vue';
import router from './router';
import store from './store';
import BaseDialog from '@/components/UI/BaseDialog.vue';
import BaseButton from '@/components/UI/BaseButton.vue';
import BaseInput from '@/components/UI/BaseInput.vue';

Vue.config.productionTip = false;

Vue.component('BaseDialog', BaseDialog);
Vue.component('BaseButton', BaseButton);
Vue.component('BaseInput', BaseInput);

new Vue({
  router,
  store,
  render: h => h(App)
}).$mount('#app');
