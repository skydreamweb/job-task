import Vue from 'vue';
import VueRouter from 'vue-router';
import Products from '@/views/ProductsPage.vue';
import Blog from '@/views/BlogPage.vue';
import Product from '@/views/SingleProductPage.vue';
import Post from '@/views/PostPage.vue';
import store from '@/store';

Vue.use(VueRouter);

const routes = [
  { path: '/', redirect: '/products' },
  {
    path: '/products',
    name: 'Products',
    component: Products
  },
  {
    path: '/products/:id',
    name: 'Product',
    component: Product,
    props: true,
    beforeEnter(to, from, next) {
      // if there are products in store
      if (store.getters.products.length > 0) {
        next();
      }
      // if there are not products in store fetch through Vuex action from DB
      store
        .dispatch('fetchProducts')
        .then(() => {
          next();
        })
        .catch(err => {
          console.log(err);
          next('/');
        });
    }
  },
  {
    path: '/blog',
    name: 'Blog',
    component: Blog
  },
  {
    path: '/blog/:id',
    name: 'Post',
    component: Post,
    props: true,
    beforeEnter(to, from, next) {
      store
        .dispatch('fetchPost', to.params.id)
        .then(post => {
          // passing post through prop (props: true)
          to.params.postData = post;
          next();
        })
        .catch(err => {
          console.log(err);
          next('/');
        });
    }
  }
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
});

export default router;
