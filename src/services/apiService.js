import axios from 'axios';

const apiClient = axios.create({
  baseURL: 'http://localhost:3000',
  withCredentials: false,
  header: {
    Accept: 'application/json',
    'Content-Type': 'application/json'
  }
});

export default {
  getProducts() {
    return apiClient.get('/products');
  },
  getProduct(id) {
    return apiClient.get(`/products/${id}`);
  },
  getPosts() {
    return apiClient.get('/posts');
  },
  getPost(id) {
    return apiClient.get(`/posts/${id}`);
  },
  editPost(payload) {
    return apiClient.patch(`/posts/${payload.id}`, payload.comments);
  },
  getCartItems() {
    return apiClient.get('/cart');
  },
  createCartItem(item) {
    return apiClient.post('/cart', item);
  },
  editCartItem(id, data) {
    return apiClient.patch(`/cart/${id}`, data);
  },
  deleteCartItem(id) {
    return apiClient.delete(`/cart/${id}`);
  }
};
